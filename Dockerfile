FROM golang:1.11 as builder
COPY . /go/src/app
WORKDIR /go/src/app
RUN go build -o kogan_challenge *.go 

FROM debian:stretch-slim
COPY --from=builder /go/src/app/kogan_challenge . 
CMD ["./kogan_challenge"]  