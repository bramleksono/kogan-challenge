Kogan.com Coding Challenge : Calculate Average Cubic Weight of Air Conditioner products

Author : Bramanto Leksono

This is a CLI programs written in Go Language and packaged as docker images.
The logic is designed as follows:
1. Set base url as "http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com/api/products/1".
2. Get data from defined url.
3. Filter product based on "Air Conditioner" category.
4. Calculate Average Cubic Weight of obtained products.
5. If next url is exists, overwrite base url with new path. Go to step 2. If not, exit the program.

Requirement:

[docker](https://docs.docker.com/install/)

To run:

```sh build_prod.sh```