package productutil

import (
	"app/physicalutil"
	"encoding/json"
)

type ProductsResponse struct {
	Products []Product `json:"objects"`
	Next     string    `json:"next"`
}

type Product struct {
	Category string                 `json:"category"`
	Title    string                 `json:"title"`
	Weight   float32                `json:"weight"`
	Size     physicalutil.Dimension `json:"size"`
}

// ParseProductResponseJson takes byte slice and returns ProductsResponse
func ParseProductResponseJson(inputByteSlice []byte) (ProductsResponse, error) {

	productResponse := ProductsResponse{}
	err := json.Unmarshal(inputByteSlice, &productResponse)
	if err != nil {
		return productResponse, err
	}

	return productResponse, nil

}

// FilterProducts returns products with matching category
func FilterProducts(filter string, inputProducts []Product) []Product {

	result := inputProducts[:0]
	for _, product := range inputProducts {
		if product.Category == filter {
			result = append(result, product)
		}
	}

	return result
}

// ConvertInputDimension takes products in cm dimension and returns products in m dimension
func ConvertInputDimension(inputProducts []Product) []Product {

	result := inputProducts[:0]
	for _, product := range inputProducts {
		product.Size.Width = product.Size.Width / 100
		product.Size.Length = product.Size.Length / 100
		product.Size.Height = product.Size.Height / 100
		result = append(result, product)
	}

	return result

}

// GetAverageCubicWeight calculate average cubic weight of given products
func GetAverageCubicWeight(inputProducts []Product) (float32, error) {

	sum := float32(0)

	for _, product := range inputProducts {
		volume, err := physicalutil.CalculateVolume(product.Size)
		if err != nil {
			return sum, err
		}
		weight, err := physicalutil.CalculateWeight(product.Category, volume)
		if err != nil {
			return sum, err
		}
		sum += weight
	}

	return sum / float32(len(inputProducts)), nil

}

// ProcessProductResponse takes ProductsResponse and returns average cubic weights
func ProcessProductResponse(productResponse ProductsResponse, filter string) (float32, error) {

	result := float32(0)

	fiteredProduct := FilterProducts(filter, productResponse.Products)

	convertedProduct := ConvertInputDimension(fiteredProduct)

	result, err := GetAverageCubicWeight(convertedProduct)
	if err != nil {
		return result, err
	}

	return result, nil

}
