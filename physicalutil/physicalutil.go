package physicalutil

import "errors"

var (
	// ErrInvalidDimension occurs when any of dimension has zero value
	ErrInvalidDimension = errors.New("dimension cannot be zero")

	// ErrInvalidCategoryName occurs when category name is not defined in WeightFactor
	ErrInvalidCategoryName = errors.New("unknown category name")
)

// Dimension represent packaging dimension
type Dimension struct {
	Width  float32 `json:"width"`
	Length float32 `json:"length"`
	Height float32 `json:"height"`
}

// validateDimension returns error if any of dimension has zero value
func validateDimension(dimension Dimension) error {
	zeroFloat := float32(0)

	if dimension.Width == zeroFloat {
		return ErrInvalidDimension
	} else if dimension.Length == zeroFloat {
		return ErrInvalidDimension
	} else if dimension.Height == zeroFloat {
		return ErrInvalidDimension
	} else {
		return nil
	}
}

// CalculateVolume returns volume from long, high, and wide measurement
func CalculateVolume(dimension Dimension) (float32, error) {
	err := validateDimension(dimension)
	if err != nil {
		return float32(0), err
	}

	volume := dimension.Width * dimension.Height * dimension.Length

	return volume, nil
}

// CalculateWeight takes volume in m^3 and returns weight in kg
func CalculateWeight(categoryName string, volume float32) (float32, error) {

	weightFactor, ok := WeightFactorConst[categoryName]

	if !ok {
		return float32(0), ErrInvalidCategoryName
	}

	weight := volume * float32(weightFactor)

	return weight, nil
}
