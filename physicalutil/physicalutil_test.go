package physicalutil

import (
	"testing"
)

func TestCalculateVolume(t *testing.T) {
	testDimension := Dimension{
		Width:  0.30,
		Height: 1,
		Length: 0.25,
	}

	expected := float32(0.075)

	actual, _ := CalculateVolume(testDimension)

	if actual != expected {
		t.Errorf("Expected the sum of %v to be %f but instead got %f!", testDimension, expected, actual)
	}
}

func TestZeroDimensionVolume(t *testing.T) {
	testDimension := Dimension{
		Width:  0,
		Height: 1,
		Length: 0.25,
	}

	_, err := CalculateVolume(testDimension)

	if err == nil {
		t.Errorf("Expected returning error for dimension %v but instead got nil", testDimension)
	}
}

func TestCalculateWeight(t *testing.T) {
	testVolume := float32(0.4)

	expected := float32(100)

	actual, _ := CalculateWeight("Air Conditioners", testVolume)

	if actual != expected {
		t.Errorf("Expected the weight of %f to be %f but instead got %f!", testVolume, expected, actual)
	}
}
