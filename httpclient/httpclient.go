package httpclient

import (
	"io/ioutil"
	"net/http"
	"net/url"
)

type HttpResponse struct {
	Body   []byte
	Status int
}

func Get(url string) (HttpResponse, error) {

	result := HttpResponse{}

	resp, err := http.Get(url)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	result = HttpResponse{
		Body:   body,
		Status: resp.StatusCode,
	}

	return result, nil

}

func GetHostFromURL(inputUrl string) (*url.URL, error) {
	u, err := url.Parse(inputUrl)
	if err != nil {
		return &url.URL{}, err
	}

	return u, nil
}
