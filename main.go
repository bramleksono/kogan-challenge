package main

import (
	"app/httpclient"
	"app/productutil"
	"fmt"
	"math"
)

func main() {

	url, err := httpclient.GetHostFromURL(BaseURL)
	if err != nil {
		panic(err)
	}

	iteration := float32(0)
	overallAvgWeight := float32(0)

	for {

		response, err := httpclient.Get(url.String())
		if err != nil {
			panic(err)
		}

		productResponse, err := productutil.ParseProductResponseJson(response.Body)
		if err != nil {
			panic(err)
		}

		averageWeight, err := productutil.ProcessProductResponse(productResponse, CategoryFilter)
		if err != nil {
			panic(err)
		}

		if !math.IsNaN(float64(averageWeight)) {

			previousAverageWeight := overallAvgWeight * iteration
			iteration += 1
			overallAvgWeight = (previousAverageWeight + averageWeight) / iteration
			fmt.Printf("AverageWeight: %f overallAverageWeight: %f", averageWeight, overallAvgWeight)
			fmt.Println()
		}

		if productResponse.Next == "" {
			break
		} else {
			url.Path = productResponse.Next
		}

	}

	overallAvgWeightGrams := overallAvgWeight * 1000

	fmt.Println("Average cubic weight (grams): ", overallAvgWeightGrams)

}
